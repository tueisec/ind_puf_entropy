% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-05-10
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [s] = binspace(width)
  % Returns a 2^width-by-width array containing all possible binary vectors
  % of width width. Used as a faster replacement for
  % dec2bin(0:2^width-1,width)-'0'
%  s = [0; 1];
%  for i=2:width
%    s = [zeros(size(s, 1), 1) s; ones(size(s, 1), 1) s];
%  end
  s = zeros(2^width, width);
  s(2, width) = 1;
  for i=2:width
    s(2^(i-1)+1:2^i,width-i+2:width) = s(1:2^(i-1),width-i+2:width);
    s(2^(i-1)+1:2^i,width-i+1) = 1;
  end
end
