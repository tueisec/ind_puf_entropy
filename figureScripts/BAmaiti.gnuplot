# Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
# This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
# the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
# Created: 2019-09-27
####################################################################################################

reset
  
set term epslatex monochrome size 8.87cm,2.05cm
set output "BAmaiti.tex"
set size ratio -1 1.0,1.0
set origin 0.0,0.0

set lmargin 2.5
set rmargin 0.0
set tmargin 0.3
set bmargin 1.1

unset key
set xrange [-1:32]
set xtics 0, 8, 32 out
set yrange [8:-1]
set ytics 0, 2, 8 out
set cbtics 0.25

plot "BAmaiti.tsv" matrix with image

unset output
