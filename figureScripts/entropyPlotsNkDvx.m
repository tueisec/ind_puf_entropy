% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-07-02
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculates the data necessary for plots of (n-k) and Delvaux bounds based
% on the Maiti et al. dataset

% load cached Maiti data
load("../maiti.mat", "F")
% extract BitAlias and log2 thereof
BAs = F.bit.sample.mean.round.device.mean.value;
lBAs = [log2(BAs') log2(1 - BAs')];
n = size(BAs, 2);

% calculate the min-entropy of the PUF itself for i.i.d. and ind. case
p_iid = mean(BAs);
m_bit = -log2(max(p_iid, 1-p_iid));
%m_iid_s = n * m_iid; better report the sum of those bits also used by ECC
m1_v = -max(lBAs, [], 2);
%m_ind_s = sum(m_ind_v); better report the sum of those bits also used by ECC

% calculate the (n-k) bound for a 5-rep code for i.i.d. and ind. case
n_b = 5;
k_b = 1;
onb = floor(n/n_b); % number of blocks
n_s = onb*n_b; % number of bits used by ECC
m = n_s * m_bit;
m1 = sum(m1_v(1:n_s));
l_rep5_nk_m = m - (n_b - k_b)*onb;
l_rep5_nk_m_v = ones(onb, 1) * (l_rep5_nk_m / onb);
l_rep5_nk_m1 = m1 - (n_b - k_b)*onb;
l_rep5_nk_m1_v = ones(onb, 1) * (l_rep5_nk_m1 / onb);
l1_rep5_nk_m1_v = sum(reshape(m1_v(1:(onb*n_b)), n_b, onb), 1)' - (n_b - k_b);
l1_rep5_nk_m1 = sum(max(l1_rep5_nk_m1_v, zeros(size(l1_rep5_nk_m1_v))));

addpath('../../')
% calculate the Delvaux bound for a 5-rep code for i.i.d. and ind. case
E = binspace(n_b);
E = E(sum(E, 2) <= n_b/2, :);
W = [zeros(1, n_b); ones(1, n_b)];
l_rep5_dvx_m_block = delvaux9(ones(n_b, 1)*[log2(p_iid) log2(1-p_iid)], E, W);
l_rep5_dvx_m_v = ones(onb, 1) * l_rep5_dvx_m_block;
l_rep5_dvx_m = sum(l_rep5_dvx_m_v);
[l1_rep5_dvx_m1, l1_rep5_dvx_m1_v] = delvaux9(lBAs, E, W);

% write to file
filename = 'entropy5REP.csv';
fid = fopen(filename, 'wt');
fprintf(fid, '%s\t%s\t%s\t%s\t%s\t%s\t%s\n', 'm', 'm1', 'l_rep5_nk_m', 'l_rep5_nk_m1', 'l1_rep5_nk_m1', 'l_rep5_dvx_m', 'l1_rep5_dvx_m1');
fclose(fid);
out = nan(n+1, 7);
out(1, :) = [m, m1, l_rep5_nk_m, l_rep5_nk_m1, l1_rep5_nk_m1, l_rep5_dvx_m, l1_rep5_dvx_m1];
out(2:end, 1:2) = [ones(n, 1) * m_bit, m1_v];
out(1+ (1:onb)*n_b-(n_b-1)/2, 3:7) = [l_rep5_nk_m_v, l_rep5_nk_m1_v, l1_rep5_nk_m1_v, l_rep5_dvx_m_v, l1_rep5_dvx_m1_v'];
dlmwrite(filename, out, '-append', 'delimiter', '\t');
