# Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
# This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
# the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
# Created: 2019-07-02
####################################################################################################

reset

set term epslatex monochrome size 8.87cm,3.7cm
set output "entropy.tex"
set size 1.0,1.0
set origin 0.0,0.0

set key tmargin horizontal autotitle columnhead samplen 2
set rmargin 1.5
set xlabel "Position in response"
set xrange [0:256]
set xtics 64 out nomirror
set ylabel "Entropy per bit"
set ytics 0.2 out
set border 11

plot 'entropy5REP.csv' every ::1 using ($0+0.5):1 with lines lw 2 title "\\acrshort{iid}",\
     'entropy5REP.csv' every ::1 using ($0+0.5):2 with boxes fs solid 0.3 noborder title "\\acrshort{ind}"

unset output
set border 31
set output "entropyNK.tex"

set key width -4
set xlabel "Response positions used"
set xrange [0:255]
set xtics 50 in mirror
set ylabel "Entropy per block"
set yrange [-1.2:1.2]
set ytics 0.5 in
set boxwidth 5

plot 'entropy5REP.csv' every 5::3 using ($0*5+2.5):4 with lines ls 3 title "\\acrshort{iid} ($\\tilde{m}$)",\
     'entropy5REP.csv' every 5::3 using ($0*5+2.5):3 with lines ls 2 title "\\acrshort{iid}",\
     'entropy5REP.csv' every ::1 using ($0+0.5):5 with boxes ls 1 title "\\acrshort{ind}"

unset output
set output "entropyDVX.tex"

set key width 0
set yrange [0:1]
set ytics 0.2

plot 'entropy5REP.csv' every 5::3 using ($0*5+2.5):6 with lines ls 2 title "\\acrshort{iid}",\
     'entropy5REP.csv' every ::1 using ($0+0.5):7 with boxes ls 1 title "\\acrshort{ind}"

unset output
