# Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
# This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
# the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
# Created: 2019-07-11
####################################################################################################

reset

set term epslatex monochrome size 8.87cm,6.99cm
set output "keyrank2.tex"
set size 1.0,1.0
set origin 0.0,0.0

set key tmargin horizontal autotitle columnhead samplen 2
set tmargin 0.3
set rmargin 1.5
set xlabel "Key Rank [\\bit]"
set xrange [0:144]
set ylabel "Count" offset 4.0,0
set yrange [0:3.8e2]
set ytics out ("(3)" 3.5e2, "(5)" 3.2e2, "(7)" 2.9e2, "(21)" 2.2e2, "(7,4,1)" 1.9e2, "(15,5,3)" 1.6e2, "(31,6,7)" 1.3e2, "(63,7,15)" 1.0e2, "(127,8,31)" 0e2)
set lmargin 10

plot 'guesswork2Keyrank.csv' using 0:($1+ 3.5e2) with steps ls 1 notitle,\
     'guesswork2Keyrank.csv' using 0:($2+ 3.2e2) with steps ls 1 notitle,\
     'guesswork2Keyrank.csv' using 0:($3+ 2.9e2) with steps ls 1 notitle,\
     'guesswork2Keyrank.csv' using 0:($4+ 2.2e2) with steps ls 1 notitle,\
     'guesswork2Keyrank.csv' using 0:($5+ 1.9e2) with steps ls 1 notitle,\
     'guesswork2Keyrank.csv' using 0:($6+ 1.6e2) with steps ls 1 notitle,\
     'guesswork2Keyrank.csv' using 0:($7+ 1.3e2) with steps ls 1 notitle,\
     'guesswork2Keyrank.csv' using 0:($8+ 1.0e2) with steps ls 1 notitle,\
     'guesswork2Keyrank.csv' using 0:($9+ 0.0e2) with steps ls 1 notitle,\
     'entropyArrows.csv' every ::::2 using 1:(3.5e2-($0*3e1)) with points pt 2 notitle,\
     'entropyArrows.csv' every ::3::7 using 1:(2.2e2-($0*3e1)) with points pt 2 notitle,\
     'entropyArrows.csv' every ::8 using 1:(0) with points pt 2 notitle,\
     'entropyArrows.csv' every ::::2 using 1:(3.5e2-($0*3e1)):(0):(2e1) with vectors nohead ls 1 notitle,\
     'entropyArrows.csv' every ::3::7 using 1:(2.2e2-($0*3e1)):(0):(2e1) with vectors nohead ls 1 notitle,\
     'entropyArrows.csv' every ::8 using 1:(0):(0):(3e1) with vectors nohead ls 1 notitle,\
     'entropyArrows.csv' every ::::2 using 2:(3.5e2-($0*3e1)) with points pt 6 notitle,\
     'entropyArrows.csv' every ::3::7 using 2:(2.2e2-($0*3e1)) with points pt 6 notitle,\
     'entropyArrows.csv' every ::8 using 2:(0) with points pt 6 notitle,\
     'entropyArrows.csv' every ::::2 using 2:(3.5e2-($0*3e1)):(0):(2e1) with vectors nohead ls 1 notitle,\
     'entropyArrows.csv' every ::3::7 using 2:(2.2e2-($0*3e1)):(0):(2e1) with vectors nohead ls 1 notitle,\
     'entropyArrows.csv' every ::8 using 2:(0):(0):(3e1) with vectors nohead ls 1 notitle

unset output
