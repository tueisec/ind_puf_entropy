% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-07-11
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Produces the data for keyrank again, but with the same keys implemented
% on every device and reporting the mean keyrank for each device

% load cached Maiti data
load('../maiti.mat', 'F');
% extract BitAlias
BAs = F.bit.sample.mean.round.device.mean.value;
lBAs = [log2(BAs') log2(1 - BAs')];
% make previous functions available
addpath('../');

% Codes that need to be processed
codes = struct();
codes.R3EP = struct('name', '(3)', 'n_b', 3, 'k_b', 1);
codes.R5EP = struct('name', '(5)', 'n_b', 5, 'k_b', 1);
codes.R7EP = struct('name', '(7)', 'n_b', 7, 'k_b', 1);
codes.R21EP = struct('name', '(21)', 'n_b', 21, 'k_b', 1);
codes.B7C4H = struct('name', '(7,4,1)', 'n_b', 7, 'k_b', 4);
codes.B15C5H = struct('name', '(15,5,3)', 'n_b', 15, 'k_b', 5);
codes.B31C6H = struct('name', '(31,6,7)', 'n_b', 31, 'k_b', 6);
codes.B63C7H = struct('name', '(63,7,15)', 'n_b', 63, 'k_b', 7);
codes.B127C8H = struct('name', '(127,8,31)', 'n_b', 127, 'k_b', 8);
codelist = fieldnames(codes);

% Create random keys only once for comparability
filename = 'guesswork2KeyAssignment.mat';
if exist(filename, 'file')
  load(filename)
else
  devices = size(F.bit.sample.mean.round.value, 3);
  samples = 1e4;
  o = nan(size(codelist));
  for i=1:length(codelist)
    o(i) = floor(size(BAs, 2) / codes.(char(codelist(i))).n_b)*codes.(char(codelist(i))).k_b;
  end
  o_max = max(o);
  keys = rand(samples, o_max) >= 0.5;
  save(filename, 'keys', 'samples', 'devices', 'o_max', '-v7.3');
end

% 1e6 samples takes too long to compute, try the first 1e5 instead
chunkSize = 1e4;

%Prepare worker threads
poolobj = gcp('nocreate');
if isempty(poolobj)
  poolobj = parpool(feature('numCores'));
elseif poolobj.NumWorkers < feature('numCores')
  delete(poolobj);
  poolobj = parpool(feature('numCores'));
end

% load previous results
filename = 'guesswork2KeyRanks.mat';
if exist(filename, 'file')
  load(filename)
else
  keyRanks = struct();
end
logKeyRankHist = nan(length(codelist), o_max+1);
for i=1:length(codelist)
  k_b = codes.(char(codelist(i))).k_b;
  n_b = codes.(char(codelist(i))).n_b;
  blocks = floor(size(BAs, 2) / n_b);
  o = blocks * k_b;
  n = blocks * n_b;
  if ~isfield(keyRanks, char(codelist(i)))
    keyRanks.(char(codelist(i))) = struct('k_b', k_b, 'n_b', n_b, 'blocks', blocks, 'o', o, 'n', n);
  end
  if ~isfield(keyRanks.(char(codelist(i))), 'ranks')% try rep-code first
    keyRanks.(char(codelist(i))).ranks = nan(samples, devices);
  end
  tbd = isnan(keyRanks.(char(codelist(i))).ranks);
  if any(any(tbd))
    % in case calculation was aborted, start after the last key that way
    % calculated for all devices
    start = nan(1, devices);
    for d=1:devices
      start(d) = find(tbd(:, d), 1, 'first');
    end
    start = min(start);
    ranks = nan(min(chunkSize, samples - start + 1), devices);
    cSamples = size(ranks, 1);
    r = keys(start:start+cSamples-1, 1:o);
    if k_b > 1 % precalculate messages and codewords for helperDataBCHcode and getBCHmessageLogProb to improve speed
      messages = binspace(k_b);
      codewordsGF = bchenc(gf(messages, 1), n_b, k_b);
      codewords = codewordsGF.x;
    end
    parfor d=1:devices
      % PUF response
      x = F.bit.sample.mean.round.value(1, 1:n, d, 1, 1);
      dranks = nan(cSamples, 1);
      for j=1:cSamples
        if k_b == 1
          % helper data
          y = helperDataRepetitionCode(x, r(j, :), n_b);
          % determine log-probabilities for each key bit given the helperdata
          lpxy = getREPoutputBias(lBAs, y, blocks, n_b);
          % determine rank of correct key
          dranks(j) = keyRankBinary(lpxy, r(j, :));
        else % BCH code, use different implementation
          y = helperDataBCHcode(x, r(j, :), n_b, k_b, messages, codewords);
          % determine the log-probabilities for all message hypotheses for
          % all ECC blocks
          lpxy = getBCHmessageLogProb(lBAs, y, blocks, n_b, k_b, messages, codewords);
          % determine rank of correct key
          dranks(j) = keyRankSubkey(lpxy, messages, r(j, :));
        end
      end
      ranks(:, d) = dranks;
    end
    keyRanks.(char(codelist(i))).ranks(start:start+cSamples-1, :) = ranks;
    keyRanks.(char(codelist(i))).meanranks = nanmean(keyRanks.(char(codelist(i))).ranks, 1);
    save(filename, 'keyRanks', '-v7.3');
  end
  logKeyRankHist(i, :) = histcounts(log2(keyRanks.(char(codelist(i))).meanranks), 0:o_max+1);
end

filename = 'guesswork2Keyrank.csv';
fid = fopen(filename, 'wt');
header = cell(size(codelist));
for i=1:length(codelist)
  header{i} = codes.(char(codelist(i))).name;
end
fprintf(fid, '%s\n', strjoin(header, '\t'));
fclose(fid);
dlmwrite(filename, logKeyRankHist', '-append', 'delimiter', '\t');
