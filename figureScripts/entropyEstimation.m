% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-06-20
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculates the data for table of entropies for several codes and several
% estimations, such as (n-k), Delvaux, groupingBound based on the Maiti et
% al. dataset

% load cached Maiti data
load("../maiti.mat", "F")
% extract BitAlias and log2 thereof
BAs = F.bit.sample.mean.round.device.mean.value;
lBAs = [log2(BAs') log2(1 - BAs')];
n = size(BAs, 2);

% calculate the min-entropy of the PUF itself for i.i.d. and ind. case
p_iid = mean(BAs);
m_bit = -log2(max(p_iid, 1-p_iid));
m1_v = -max(lBAs, [], 2);

addpath('../')

filename = 'entropyEstimate.mat';
if ~exist(filename, 'file')
  names = {'(3)'; '(5)'; '(7)'; '(21)'; '(7,4,1)'; '(15,5,3)'; '(31,6,7)'; '(63,7,15)'; '(127,8,31)'};
  %names = {'(3)'; '(5)'; '(7)'; '(21)'; '(7,4,1)'; '(15,5,3)'; '(31,6,7)'; '(63,7,15)'};
  n_bs = [3; 5; 7; 21; 7; 15; 31; 63; 127];
  k_bs = [1; 1; 1; 1; 4; 5; 6; 7; 8];
  n_ss = nan(size(names));
  k_ss = nan(size(names));
  ms = nan(size(names));
  m1s = nan(size(names));
  l_nk_ms = nan(size(names));
  l_nk_m1s = nan(size(names));
  l1_nk_m1s = nan(size(names));
  l_dvx_ms = nan(size(names));
  l1_dvx_m1s = nan(size(names));
  groupDeltas = [0.05 0.1];
  l1_grp_m1s = nan(size(names, 1), length(groupDeltas));
  groupingTimes = nan(size(l1_grp_m1s));
  groupMaxCounts = nan(size(l1_grp_m1s));
  groupSizeMeans = nan(size(l1_grp_m1s));

  for i=1:size(names, 1)
    n_b = n_bs(i);
    k_b = k_bs(i);
    onb = floor(n/n_b); % number of blocks
    n_s = onb*n_b; % number of bits used by ECC
    n_ss(i) = n_s;
    k_ss(i) = onb*k_b; % length of random number = maximum security level
    ms(i) = n_s * m_bit;
    m1s(i) = sum(m1_v(1:n_s));
    % (n-k) bound
    l_nk_ms(i) = ms(i) - (n_b - k_b)*onb;
    l_nk_m1s(i) = m1s(i) - (n_b - k_b)*onb;
    l1_nk_m1 = sum(reshape(m1_v(1:n_s), n_b, onb), 1)' - (n_b - k_b);
    l1_nk_m1s(i) = sum(max(l1_nk_m1, zeros(size(l1_nk_m1))));
    % Avg. conditional min-entropy
    if n_b < 22
      if k_b == 1
        E = binspace(n_b);
        E = E(sum(E, 2) <= n_b/2, :);
        W = [zeros(1, n_b); ones(1, n_b)];
      else
        [E, W] = getBCHerrorVectors(n_b, k_b);
      end
      l_dvx_ms(i) = onb * delvaux9(ones(n_b, 1)*[log2(p_iid) log2(1-p_iid)], E, W);
      l1_dvx_m1s(i) = delvaux9(lBAs, E, W);
    end
    % Grouping bound
    for di=1:length(groupDeltas)
      etas = cell(onb, 1);
      Hs = zeros(onb, 1);
      stime = cputime;
      for b=0:onb-1
        [Hs(b+1), etas{b+1}] = groupingBound(BAs((b*n_b)+1:(b+1)*n_b), n_b, k_b, groupDeltas(di));
      end
      l1_grp_m1s(i, di) = sum(Hs);
      groupingTimes(i, di) = cputime - stime;
      groupMaxCounts(i, di) = max(cellfun(@length, etas));
      groupSizeMeans(i, di) = mean(cellfun(@mean, etas));
    end
  end
  save(filename, 'names', 'n_bs', 'k_bs', 'n_ss', 'k_ss', 'ms', 'm1s', 'l_nk_ms', 'l_nk_m1s', 'l1_nk_m1s', 'l_dvx_ms', 'l1_dvx_m1s', 'groupDeltas', 'l1_grp_m1s', 'groupingTimes', 'groupMaxCounts', 'groupSizeMeans', '-v7.3')
else
  load(filename)
end

% write to file
filename = 'entropyTable.tex';
fid = fopen(filename, 'wt');
header = {'', '\multicolumn{1}{c}{$n$}', '\multicolumn{1}{c}{$m$}', '\multicolumn{1}{c}{$m''$}', '\multicolumn{1}{c}{$k$}', '\multicolumn{1}{c}{$l$}', '\multicolumn{1}{c}{$l (m'')$}', '\multicolumn{1}{c}{$l''$}', '\multicolumn{1}{c}{\acrshort{iid}}', '\multicolumn{1}{c}{\acrshort{ind}}'};
for d=1:length(groupDeltas)
%  header{end+1} = sprintf('\\multicolumn{1}{c}{$\\Delta \\theta_\\tau=%4.2f$}', groupDeltas(d));
  header{end+1} = sprintf('%4.2f', groupDeltas(d));
end
fprintf(fid, '%s \\\\\n\\hline\n', strjoin(header, ' & '));
fprintf(fid, ' & %3.0f & %3.0f & %3.0f &  &  &  &  &  & \\\\\n', size(m1_v, 1), size(m1_v, 1)*m_bit, sum(m1_v));
for i=1:size(names, 1)
  % adapt printf formatting to always print exactly 3 significant digits
  % codename is %s; n, m and m' are always above hundred, so we can fix the
  % 3.0 format; k is between 12 and 146, but since it is an integer by
  % definition, we can also keep the 3.0 format
  format = {'%s', '%3.0f', '%3.0f', '%3.0f', '%3.0f'};
  % for the other numbers automatically calculate how many decimal digits
  % are allowed to print exactly 3 significant digits
  intfigs = max(ceil(log10(abs([l_nk_ms(i), l_nk_m1s(i), l1_nk_m1s(i), l_dvx_ms(i), l1_dvx_m1s(i), l1_grp_m1s(i, :)]))), 1);
  decfigs = 3 - intfigs;
  for j=1:size(intfigs, 2)
    format{end+1} = sprintf('%%.%if', decfigs(j));
  end
  fprintf(fid, strcat(strjoin(format, ' & '), ' \\\\\n'), char(names(i)), n_ss(i), ms(i), m1s(i), k_ss(i), l_nk_ms(i), l_nk_m1s(i), l1_nk_m1s(i), l_dvx_ms(i), l1_dvx_m1s(i), l1_grp_m1s(i, :));
end
fclose(fid);

filename = 'groupingTime.csv';
fid = fopen(filename, 'wt');
header = [{'theta_delta'} names'];
fprintf(fid, '%s\n', strjoin(header, '\t'));
fclose(fid);
dlmwrite(filename, [groupDeltas' groupingTimes'], '-append', 'delimiter', '\t');
fid = fopen(filename, 'at');
fprintf(fid, '\n');
fclose(fid);
dlmwrite(filename, [groupDeltas' groupMaxCounts'], '-append', 'delimiter', '\t');
fid = fopen(filename, 'at');
fprintf(fid, '\n');
fclose(fid);
dlmwrite(filename, [groupDeltas' groupSizeMeans'], '-append', 'delimiter', '\t');

filename = 'entropyArrows.csv';
fid = fopen(filename, 'wt');
header = [{'k'}, {'groupingBound0.05'}, {'groupingBound0.1'}];
fprintf(fid, '%s\n', strjoin(header, '\t'));
fclose(fid);
dlmwrite(filename, [k_ss l1_grp_m1s], '-append', 'delimiter', '\t');
