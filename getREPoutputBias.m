% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-07-06
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [glp, e] = getREPoutputBias(logBitAlias, h, k, n_rep)
  glp = nan(2, k);
  for i=0:k-1 % faster than a matrix multiplication, because we only need the trace of the resulting matrix
    hs(2,:) = h(n_rep*i + (1:n_rep));
    lba = logBitAlias(n_rep*i + (1:n_rep),:);
    hs(1,:) = 1 - hs(2,:); % so the log-probability for h~=w is calculated, too
    glp(:,i+1) = hs * lba(:,1) + (1 - hs) * lba(:,2);
  end
  % scale to have actual probabilities
  % If the ratio between P(0) and P(1) for each bit should remain the same,
  % then the difference of the log-probabilities has to stay the same, so
  % we have to add the same value to both log-probabilities for scaling
  % We want P(0)+P(1)=1 for each bit, thus we have to multiply with
  % 1/(P(0)+P(1)) respectively add -log2(2^lp(0)+2^lp(1))
  glp = glp - ([1; 1] * log2(sum(2.^glp, 1)));
  e = sum(2.^glp, 1) - 1;
end
