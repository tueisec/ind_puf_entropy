% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-04-11
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [rank, lowerBound, upperBound] = keyRankBinary(logProbs, key)
  % Calculates an estimation of the rank of key key according to Glowacz et
  % al. "Simpler and More Efficient Rank Estimation for Side-Channel
  % Security Assessment". logProbs must contain the logarithmic
  % probabilities for a 1 (first row) and a 0 (second row) for each bit
  % position in key.
  
  % Check that logProbs is a 2-by-n matrix and key is a 1-by-n row-vector
  if size(logProbs, 1) ~= 2 || size(key, 1) ~= 1 || size(logProbs, 2) ~= size(key, 2)
    error('Arguments must have same number of columns and two respectively one row!');
  end
  
  % Split logProbs into subspaces that are feasible to enumerate. To
  % support arbitrary key lengths, allow for two different subKey lengths
  maxSubLen = 16;
  subSpaceCounts = [ceil(size(logProbs, 2)/maxSubLen) 0];
  subSpaceLens = [0; 1] + floor(size(logProbs, 2) / subSpaceCounts(1));
  subSpaceCounts(2) = size(logProbs, 2) - subSpaceCounts * subSpaceLens;
  subSpaceCounts(1) = subSpaceCounts(1) - subSpaceCounts(2);

  subKeyLogProbs1 = bitLP2allSymbolLP(reshape(logProbs(:, 1:subSpaceCounts(1)*subSpaceLens(1)), 2, subSpaceLens(1), subSpaceCounts(1)));
  subKeyLogProbs2 = bitLP2allSymbolLP(reshape(logProbs(:, subSpaceCounts(1)*subSpaceLens(1)+1:end), 2, subSpaceLens(2), subSpaceCounts(2)));

  % Create histograms for all parts of the keyspace and convolute them
  minSubKeyLogProb = min([min(subKeyLogProbs1) min(subKeyLogProbs2)]);
  maxSubKeyLogProb = max([max(subKeyLogProbs1) max(subKeyLogProbs2)]);
  binEdges = linspace(minSubKeyLogProb, maxSubKeyLogProb, 2^(subSpaceLens(1)-4)+1);
  counts = histcounts(subKeyLogProbs1(:,1), binEdges);
  for i=2:subSpaceCounts(1)
    counts = conv(counts, histcounts(subKeyLogProbs1(:,i), binEdges));
  end
  for i=1:subSpaceCounts(2)
    counts = conv(counts, histcounts(subKeyLogProbs2(:,i), binEdges));
  end
  % Calculating the location of the bins resulting from convolution, one
  % has to use the bin centers. In the very first bin will only be those
  % samples that where in the lowest bin for every subKey, thus their
  % probability is approximately the central value of the lowest bin times
  % the number of histograms. If they are in the second lowest bin just
  % once, they end up in the second lowest bin, thus the bin width is
  % unaffected by convolution. The location of the uppermost bin can be
  % calculated similarly to that of the lowest bin.
  binWidth = binEdges(2) - binEdges(1);
  binCenters = (sum(subSpaceCounts) * (binEdges(1) + binWidth/2)) + ((1:size(counts, 2))-1) * binWidth;
  overallEdges = binCenters - binWidth/2; % contains only left edges, i.e. not the right edge of the uppermost bin!

  % calculate position of given key in overall probability distribution
  keyLogProb = key * logProbs(1,:)' + (1 - key) * logProbs(2,:)';
  keyBin = find(overallEdges <= keyLogProb, 1, 'last');
  % for rank, sum all keys more probable then given key
  rank = max(sum(counts(keyBin:end)), 1);
  lowerBound = max(sum(counts(min(keyBin+sum(subSpaceCounts),end):end)), 1);
  upperBound = max(sum(counts(max(keyBin-sum(subSpaceCounts),1):end)), 1);
end
