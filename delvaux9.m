% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-06-20
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [H, Hblock, n, o] = delvaux9(logProbs, errorSpace, codewordSpace)
  % Calculates the bound presented in formula (9) of Delvaux et al.
  % "Efficient Fuzzy Extraction of PUF-Induced Secrets: Theory and
  % Applications" by direct evaluation for the Maiti et al. dataset

  assert(size(logProbs, 2) == 2, "logProbs must have two columns with the log-probability of 1 and 0 (in that order)");
  assert(size(logProbs, 1) >= size(codewordSpace, 2), "PUF must have at least as many bits as one block of ECC");
  assert(size(errorSpace, 2) == size(codewordSpace, 2), "errorSpace and codewordSpace must contain row-vectors of same length");
  E = errorSpace; % shorthand notation
  W = codewordSpace; % shorthand notation
  n_b = size(W, 2); % length of a codeword = length of a block
  n_w = size(W, 1); % number of codewords = 2^<message bits per block>
  n_c = floor(size(logProbs, 1) / n_b); % number of blocks
  o = ceil(n_c * log2(n_w)); % length of output key
  n = n_c * n_b; % number of PUF bits used by the fuzzy extractor
  % Reshape logProbs for easy matrix multiplication
  blockLprob = reshape(logProbs(1:n, :), n_b, n_c, 2);

  % This calculates the log-probability that the PUF response equals some
  % helper data (=error vector XOR codeword) for each possible helperdata
  % in each ECC block. In the result, row refers to an error vector, column
  % to a block, and sheet to a codeword.
  helperDataLogProb = zeros(size(E, 1), n_c, n_w);
  for i=1:n_w
    helperData = xor(E, W(i,:));
    helperDataLogProb(:,:,i) = helperData * blockLprob(:,:,1) + (1-helperData) * blockLprob(:,:,2);
  end

  % Now we choose the codeword w that corresponds to the most probable PUF
  % response for each possible error vector, transfer to the non-log domain
  % and sum up over all possible error vectors, and go back into the log
  % domain. That gives the average (among error vectors) conditional
  % min-entropy for each block. Then we just sum up over the blocks.
  Hblock = -log2(sum(2.^max(helperDataLogProb, [], 3), 1));
  H = sum(Hblock, 2);
end
