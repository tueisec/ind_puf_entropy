% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-06-22
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [h] = helperDataRepetitionCode(puf, secret, n_rep)
  assert(isrow(secret), "secret must be a row vector");
  assert(isscalar(n_rep), "n_rep must be a scalar");
  assert(isrow(puf), "puf must be a row vector");
  c = repelem(secret, 1, n_rep);
  h = xor(c, puf(1:size(c, 2)));
end
