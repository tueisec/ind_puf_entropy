% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-07-02
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [flips] = findFlipsWithPenalty(maxPenalty, minPenalty, flipPenalties, eta)
  % Calculates recursively all possible flip vectors that end up in a
  % penalty larger than minPenalty and at most maxPenalty. The penalty of a
  % bit flip is given in flipPenalties individually per group. The maximum
  % number of bit flips in a group is eta.
  assert(isscalar(maxPenalty) && isscalar(minPenalty) && isrow(flipPenalties) && isrow(eta) && length(flipPenalties) == length(eta), "Arguments maxPenalty and minPenalty must be scalar, flipPenalties and eta must be row vectors of same size")
  assert(all(flipPenalties > 0), "All flipPenalties must be positive");
  
  if length(eta) > 1
    [chosenPenalty, chosenGroupIdx] = max(flipPenalties);
    chosenEta = eta(chosenGroupIdx);
    remGroups = (1:length(eta)) ~= chosenGroupIdx;
    minflips = max(0, ceil((minPenalty - (eta(remGroups) * flipPenalties(remGroups)'))/chosenPenalty));
    maxflips = min(floor(maxPenalty/chosenPenalty), chosenEta);
    flips = zeros(0, length(eta));
    for chosenFlip=minflips:maxflips
      penaltyDecrease = chosenFlip*chosenPenalty;
      imaxPenalty = maxPenalty - penaltyDecrease;
      iminPenalty = minPenalty - penaltyDecrease;
      % reduce the number of recursions by skipping all bits that have a
      % higher penalty than remaining
      fixedGroups = flipPenalties > imaxPenalty;
      remGroups = and(remGroups, ~fixedGroups);
      if any(remGroups)
        iflipPenalties = flipPenalties(remGroups);
        ieta = eta(remGroups);
        otherflips = findFlipsWithPenalty(imaxPenalty, iminPenalty, iflipPenalties, ieta);
        newIdx = size(flips, 1) + 1;
        flips = [flips; zeros(size(otherflips, 1), size(flips, 2))];
        flips(newIdx:end, remGroups) = otherflips;
      else
        newIdx = size(flips, 1) + 1;
        flips = [flips; zeros(1, size(flips, 2))];
      end
      flips(newIdx:end, chosenGroupIdx) = chosenFlip;
    end
  else
    flips = (max(0, ceil(minPenalty/flipPenalties)):min(floor(maxPenalty/flipPenalties), eta))';
    if ~isempty(flips) && flips(1) * flipPenalties <= minPenalty
      flips = flips(2:end);
    end
  end
end
