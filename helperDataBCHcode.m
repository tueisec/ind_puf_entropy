% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-07-06
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [h] = helperDataBCHcode(puf, secret, n, k, messages, codewords)
  assert(isrow(secret), "secret must be a row vector");
  assert(isscalar(n) && isscalar(k), "n and k must be scalars");
  assert(isrow(puf), "puf must be a row vector");
  secretBlocks = reshape(secret, k, [])'; % split secret into chunks of size k
  if nargin <= 4
    Wf = bchenc(gf(secretBlocks, 1), n, k);
    W = Wf.x;
  else
    assert(all(size(messages) == [2^k, k]), "messages must be the list of all possible input messages, one per row");
    assert(all(size(codewords) == [2^k, n]), "codewords must be the list of all codewords, one per row");
    [found, idx] = ismember(secretBlocks, messages, 'rows');
    assert(all(found), "secret must contain only parts that can be found in messages");
    W = codewords(idx, :);
  end
  h = xor(reshape(W', 1, []), puf(1:numel(W)));
end
