% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-05-10
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [symbolLogProbs] = bitLP2allSymbolLP(logProbs)
  % Takes a three dimensional array, where the first row contains the
  % log-probability for 1, the second row the log-probability for 0 and
  % the length of a row determines the length of a subkey. The third
  % dimension determines how many subkeys there are in the whole key. It is
  % simply iterated over. Returns a matrix with the log-probability of each
  % subkey hypothesis in a column vector. One column per subkey.

  if size(logProbs, 3) == 0 || size(logProbs, 2) == 0
    symbolLogProbs = nan(2^size(logProbs, 2)-1, 0);
    return
  end
  keys = binspace(size(logProbs, 2));
  logProbs = permute(logProbs, [2 1 3]);
  symbolLogProbs = nan(size(keys, 1), size(logProbs, 3));
  for i=1:size(logProbs, 3)
    symbolLogProbs(:,i) = keys * logProbs(:,1,i) + (1-keys) * logProbs(:,2,i);
  end
end
