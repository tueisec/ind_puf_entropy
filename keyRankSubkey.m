% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-07-06
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [rank, lowerBound, upperBound] = keyRankSubkey(logProbs, subkeys, key, binWidth)
  % Calculates an estimation of the rank of key key according to Glowacz et
  % al. "Simpler and More Efficient Rank Estimation for Side-Channel
  % Security Assessment". subkeys must contain all hypotheses for a subkey,
  % one per row, as a binary sequence. logProbs must have as many columns
  % as there are subkeys in the key. Each column must contain, for its
  % respective position in the key, the logarithmic probability of each
  % hypothesis in subkeys.

  assert(size(subkeys, 1) == size(logProbs, 1), "logProbs must contain the log-probability for each subkey hypothesis")
  assert(size(subkeys, 2)*size(logProbs, 2) == size(key, 2), "The number of columns in logProbs must match the number of subkeys in key")
  assert(size(unique(subkeys, 'rows'), 1) == size(subkeys, 1), "The list of subkey hypotheses subkeys must not contain duplicate entries")

  minSubkeyLogProb = min(min(logProbs, [], 1));
  maxSubkeyLogProb = max(max(logProbs, [], 1));
  if nargin > 3
    assert(isscalar(binWidth) && binWidth > 0, "binWidth must be a positive scalar")
    % add one binWidth to upper end to make sure the last entry (upper end of top bin) is above maxSubkeyLogProb
    binEdges = minSubkeyLogProb:binWidth:(maxSubkeyLogProb+binWidth);
    if binEdges(end-1) >= maxSubkeyLogProb
      binEdges(end) = []; % if coincidentally the range was an exact multiple of binWidth, our addition of binWidth created a superfluous bin that will never get any datapoints and can thus be removed
    end
  elseif nargin == 3
    % if no binWidth was specified, choose it so every bin gets on average 3 data points
    binEdges = linspace(minSubkeyLogProb, maxSubkeyLogProb, size(subkeys, 1));
    binWidth = binEdges(2) - binEdges(1);
  end

  key = reshape(key, size(subkeys, 2), [])'; % Matlab fills column-wise, so we need to use the transpose when reshaping
  [found, idx] = ismember(key, subkeys, 'rows');
  assert(all(found), "key must be comprised of entries in subkeys")
  keyLogProb = nan(1, length(idx));
  for i=1:length(idx)
    keyLogProb(i) = logProbs(idx(i), i);
  end
  keyLogProb = sum(keyLogProb);

  % Create histograms for all parts of the keyspace and convolute them
  counts = histcounts(logProbs(:,1), binEdges);
  for i=2:size(logProbs, 2)
    counts = conv(counts, histcounts(logProbs(:,i), binEdges));
  end
  % Calculating the location of the bins resulting from convolution, one
  % has to use the bin centers. In the very first bin will only be those
  % samples that where in the lowest bin for every subkey, thus their
  % probability is approximately the central value of the lowest bin times
  % the number of histograms. If they are in the second lowest bin, they
  % ended up just once in the second lowest bin, thus the bin width is
  % unaffected by convolution. The location of the uppermost bin can be
  % calculated similarly to that of the lowest bin.
  binCenters = (size(logProbs, 2) * (binEdges(1) + binWidth/2)) + (0:(size(counts, 2)-1)) * binWidth;
  overallEdges = binCenters - binWidth/2; % contains only left edges, i.e. not the right edge of the uppermost bin!

  % calculate position of given key in overall probability distribution
  keyBin = find(overallEdges <= keyLogProb, 1, 'last');
  % for rank, sum all keys more probable then given key
  rank = max(sum(counts(keyBin:end)), 1);
  lowerBound = max(sum(counts(min(keyBin+size(logProbs, 2),end):end)), 1);
  upperBound = max(sum(counts(max(keyBin-size(logProbs, 2),1):end)), 1);
end
