% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-07-06
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [glp, messages, e] = getBCHmessageLogProb(logBitAlias, helperdata, blocks, n, k, messages, codewords)
  if nargin < 7 % bchenc takes a long time, so speed up by allowing to pass precalculated messages and codewords
    if nargin < 6
      messages = binspace(k);
    end
    codewordsGF = bchenc(gf(messages, 1), n, k);
    codewords = codewordsGF.x;
  end

  glp = nan(2^k, blocks);
  for i=0:blocks-1 % faster than a matrix multiplication, because we only need the trace of the resulting matrix
    pufHypotheses = xor(codewords, helperdata(i*n + (1:n)));
    lBA = logBitAlias(i*n + (1:n),:);
    glp(:,i+1) = pufHypotheses * lBA(:,1) + (~pufHypotheses) * lBA(:,2);
  end
  % scale to have actual probabilities
  % If the ratio between the hypotheses should remain the same, then the
  % difference of the log-probabilities has to stay the same, so we have
  % to add the same value to all log-probabilities for scaling.
  % The probabilities must sum up to 1 for each block, thus we have to
  % multiply with 1/sum(probs) respectively add -log2(sum(2^logProbs))
  glp = glp - repmat(log2(sum(2.^glp, 1)), size(glp, 1), 1);
  e = sum(2.^glp, 1) - 1;
end
