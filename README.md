# IND_PUF_Entropy

This repository contains code and auxiliary data for the publication "Efficient Bound for Conditional Min-Entropy of Physical Unclonable Functions Beyond IID", permanent ID 3b0bccc028d2b79fcd67c668263243ce13b6112d, that was presented in 2019 on the IEEE Workshop on Information Forensics and Security in Delft, Netherlands.

The implementations expect a Bit-Alias vector as most important input, which you may calculate from PUF test data using our [PUF Quality Assessment Suite PQAS](https://gitlab.lrz.de/tueisec/PQAS).

# License
The Matlab edition is licensed under the Mozilla Public License 2.0, for details see the LICENSE file.
