% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-07-02
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [E, W, t] = getBCHerrorVectors(n, k)
  % Returns all binary vectors necessary to construct the binary space with
  % n bits for given BCH code. Optionally also returns the codewords and
  % the guaranteed error correction capability.

  Wf = bchenc(gf(binspace(k), 1), n, k);
  W = single(Wf.x);
  t = bchnumerr(n, k);
  % We can correct all error vectors up to bchnumerr(n_b, k_b), so add
  % them at once
  Y = binspace(n);
  E = Y(sum(Y, 2) <= t, :);
  % Additionally, there are error vectors with higher Hamming weight that
  % can only be corrected if decently located, so add them individually
  X = zeros(size(W, 1) * size(E, 1), n);
  for e=1:size(E, 1)
    X((e-1)*size(W, 1)+(1:size(W, 1)), :) = xor(W, E(e, :));
  end
  Eweight = t;
  Ecand = zeros(0, n);
  j = 1;
  while size(X, 1) < size(Y, 1)
    if j >= size(Ecand, 1)
      Eweight = Eweight + 1;
      Ecand = Y(sum(Y, 2) == Eweight, :);
      j = 1;
    else
      j = j + 1;
    end
    % since the 0..0 vector is always part of the codeword space, we can
    % directly check if the error vector candidate is already in the
    % helperdata space
    if ~ismember(Ecand(j,:), X, 'rows')
      E = [E; Ecand(j,:)];
      X = [X; xor(W, Ecand(j,:))];
    end
  end
  assert(size(unique(X, 'rows', 'first'), 1) == size(X, 1), 'Error vector search for BCH code failed!');
end
