% Copyright: 2019 Technical University of Munich, Florian Wilde <florian.wilde@tum.de>
% This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
% the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
% Created: 2019-06-30
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Hmin, eta, theta] = groupingBound(p_vec, n, k, p_DeltaGroup)
  % Calculates a lower bound on average conditional min-entropy for the
  % ind. case for large codes, where direct evaluation is infeasible, by
  % grouping positions with approximately equal probability for a 1 into
  % groups and then using methods from the i.i.d. case. Currently processes
  % only one block!
  
  assert(sum(size(p_vec) ~= 1) == 1 && numel(p_vec) == n, "Argument p_vec must be a vector of length n!")
  assert(isscalar(n) && isscalar(k) && isscalar(p_DeltaGroup), "Arguments n, k, p_DeltaGroup must be scalars!");

  % Preprocessing to make all p_i >= 0.5
  p_vec = max(p_vec, 1-p_vec);

  % We need 2^(n-k) entries to cover the entire helper data space
  entriesRequired = 2^(n-k);
  % Calculate group probabilities and sizes. Since min-entropy changes most
  % strongly in the area close to 0.5, align the group bounds such that the
  % last split (if max(p_vec)-min(p_vec) is not an integer multiple of
  % p_DeltaGroup) forms a group closer than p_DeltaGroup to 0.5.
  theta_lim = fliplr(max(p_vec):-p_DeltaGroup:min(p_vec));
  [eta, ~, binIdx] = histcounts(p_vec, [min(p_vec), theta_lim]);
  theta = nan(size(eta));
  for i=1:length(theta) % set theta to the maximum within each group to reduce the error compared to using the bin edge
    if eta(i) > 0
      theta(i) = max(p_vec(binIdx == i));
    end
  end
  nonEmptyGroups = eta > 0;
  theta = theta(nonEmptyGroups);
  eta = eta(nonEmptyGroups);
  thetaLog = log2([theta; 1 - theta]); % since all p_i>0.5, theta(1,:)>theta(2,:)
  bestGuessLog = thetaLog(1,:) * eta';
  flipPenalty = thetaLog(2,:)-thetaLog(1,:);
  % We need to find the 2^n-k most probably PUF responses. Since intlinprog
  % is slow and does not support <, but only <=, we enumerate all possible
  % flips within a certain range and increase the range until we found
  % enough flip vectors.
  % To avoid excessively large entryList, we compress the current
  % entryList into a superEntry that just holds the accumulated number of
  % entries and the penalty product entryList(:,1)' * 2.^(entryList(:,2))
  % once the entryList became too long
  superEntries = zeros(0, 2);
  entriesLeft = entriesRequired; % holds the number of entries required additionally to the superEntries
  if length(eta) == 1
    % if we are lucky and all bits fall into the same group, we can use a
    % shortcut, no need to use superEntries here
    fc = 1;
    entryList = [1, bestGuessLog; zeros(eta, 2)];
    while sum(entryList(:,1)) < entriesLeft
      newflips = [bestGuessLog + (fc * flipPenalty'), fc];
      entryList(fc+1, :) = [cardinalityPhi(newflips(:,2:end), eta), newflips(:,1)];
      fc = fc + 1;
    end
  else
    iterPenalty = max(-flipPenalty); % increase range by the penalty of flipping one of the most biased bits
    currentPenalty = 0;
    entryList = [1, bestGuessLog; zeros(1e3, 2)];
    j = 1;
    while sum(entryList(:,1)) < entriesLeft
      newflips = findFlipsWithPenalty(currentPenalty+iterPenalty, currentPenalty, -flipPenalty, eta);
      if ~isempty(newflips) % avoid error when no flips could be found in this range
        newflips = sortrows([bestGuessLog + (newflips * flipPenalty'), newflips], 1, 'descend');
        if size(newflips, 1)+j <= size(entryList, 1) % we have space left, fill with newflips
          entryList(j+(1:size(newflips, 1)), :) = [cardinalityPhi(newflips(:,2:end), eta), newflips(:,1)];
        else  % we do not have enough space left in entryList, compress to superEntry
          superEntries(end+1, :) = [sum(entryList(:,1)), entryList(:,1)' * 2.^(entryList(:,2))];
          j = 0;
          entriesLeft = entriesRequired - sum(superEntries(:,1));
          if size(newflips, 1) > size(entryList(:,1)) % our new entryList is even larger than the initial list, so just use it as new entryList (can do because we just compressed all previous entries into a superEntry)
            % we cannot compress the entire entryList to a superEntry here, because it might be that this is the last round and we have to strip the last few entries
            entryList = [cardinalityPhi(newflips(:,2:end), eta), newflips(:,1)];
          else
            entryList(1:size(newflips, 1), :) = [cardinalityPhi(newflips(:,2:end), eta), newflips(:,1)];
          end
        end
        j = j + size(newflips, 1);
      end
      currentPenalty = currentPenalty+iterPenalty;
    end
  end
  % cut the entryList such that we have exactly 2^(n-k) PUF responses in
  % our lists
  cumEntries = cumsum(entryList(:,1));
  lastFullIdx = find(cumEntries <= entriesLeft, 1, 'last');
  entryList(lastFullIdx+1, 1) = entriesLeft - cumEntries(lastFullIdx);
  entryList((lastFullIdx+2):end, :) = [];

  Hmin = -log2(sum(superEntries(:,2)) + entryList(:,1)' * 2.^(entryList(:,2)));
end

function [cardPhi] = cardinalityPhi(flips, eta)
  % Calculates how many PUF responses exist that have flips bits in eta
  % flipped, while flips and eta are vectors of same size
  cardPhi = exp(sum(gammaln(eta + 1) - gammaln(flips + 1) - gammaln(eta - flips + 1), 2));
end
